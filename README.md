# lakefs

## How to reproduce
1. Clone repository:
    ```bash
    git clone git@gitlab.com:kodzok/lakefs.git 
    ```
2. Install dependencies:
   ```bash
   cd lakefs
   pdm install
   eval $(pdm venv activate)
   ```
3. Run docker containers:
   ```bash
   docker compose up -d
   ```
4. Install and configure `lakectl`:
   ```bash
   lakectl config
    # output:
    # Config file /home/janedoe/.lakectl.yaml will be used
    # Access key ID: 
    # Secret access key: 
    # Server endpoint URL: http://localhost:8000
   ```
5. Run snakemake pipeline:
   ```bash
   snakemake --cores 1
   ```

## Directed Acyclic Graph
  The Directed Acyclic Graph (DAG) below illustrates the dependencies between the steps of the pipeline. Each node represents a step, and the arrows indicate the flow of data between them.

<p align="center">
  <img src="images/dag.svg">
</p>
