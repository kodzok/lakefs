configfile: "config.yaml"

rule all:
    input:
        f"{config['data_dir']}/iris_version1.csv",
        f"{config['data_dir']}/iris_version2.csv"

rule create_repo:
    output:
        f"{config['data_dir']}/repo_created.txt"
    params:
        repo_name=config['repo_name'],
        branch_name=config['branch'],
        storage_namespace=config['storage_namespace'],
        repo_uri=f"lakefs://{config['repo_name']}"
    log:
        "logs/create_repo.log"
    shell:
        """
        if lakectl repo list | grep -q {params.repo_name};
        then
            echo "Repository {params.repo_name}" already exists > {output};
        else
            lakectl repo create -d {params.branch_name} {params.repo_uri} {params.storage_namespace} &&
            echo "Repository {params.repo_name} created" > {output};
        fi
        """

rule create_data:
    output:
        f"{config['data_dir']}/iris.csv"
    log:
        "logs/create_data.log"
    shell:
        "python src/lakefs/load_data.py --save-path {output}"

rule upload_data:
    input:
        data=f"{config['data_dir']}/iris.csv",
        repo=f"{config['data_dir']}/repo_created.txt"
    output:
        first=f"{config['data_dir']}/first_commit_info.txt",
        second=f"{config['data_dir']}/second_commit_info.txt"
    params:
        branch_uri=f"lakefs://{config['repo_name']}/{config['branch']}"
    log:
        "logs/upload_data.log"
    shell:
        """
        lakectl fs upload --source {input.data} {params.branch_uri}/iris.csv
        lakectl commit {params.branch_uri} --message "initial version of iris.csv"
        lakectl branch show {params.branch_uri} | grep 'Commit ID' | awk '{{print $3}}' > {output.first}

        python src/lakefs/preprocess.py --data-path {input.data}

        lakectl fs upload --source {input.data} {params.branch_uri}/iris.csv
        lakectl commit {params.branch_uri} --message "modified version of iris.csv"
        lakectl branch show {params.branch_uri} | grep 'Commit ID' | awk '{{print $3}}' > {output.second}
        """

rule read_version1:
    input:
        f"{config['data_dir']}/first_commit_info.txt"
    output:
        f"{config['data_dir']}/iris_version1.csv"
    params:
        repo_uri=f"lakefs://{config['repo_name']}"
    log:
        "logs/read_data.log"
    shell:
        "lakectl fs download {params.repo_uri}/$(cat {input})/iris.csv {output}"

rule read_version2:
    input:
        f"{config['data_dir']}/second_commit_info.txt"
    output:
        f"{config['data_dir']}/iris_version2.csv"
    params:
        repo_uri=f"lakefs://{config['repo_name']}"
    log:
        "logs/read_data.log"
    shell:
        "lakectl fs download {params.repo_uri}/$(cat {input})/iris.csv {output}"
