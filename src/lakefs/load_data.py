from pathlib import Path

import click
import pandas as pd
from sklearn.datasets import load_iris


@click.command()
@click.option(
    "--save-path",
    "-s",
    required=True,
    type=str,
    help="The full path where the CSV file will be saved.",
)
def load_data(save_path: str) -> None:
    """
    This function loads the iris dataset from sklearn, converts it into a pandas DataFrame,
    and saves it as a CSV file ile at the specified path.

    Args:
        save_path (str): The full path where the CSV file will be saved.
    """

    iris = load_iris()
    df = pd.DataFrame(iris.data, columns=iris.feature_names)
    df["target"] = iris.target

    Path(save_path).parent.mkdir(parents=True, exist_ok=True)
    df.to_csv(save_path, index=False)


if __name__ == "__main__":
    load_data()
