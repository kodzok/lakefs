import click
import pandas as pd
from sklearn.preprocessing import StandardScaler


@click.command()
@click.option(
    "--data-path",
    "-d",
    required=True,
    type=str,
    help="The path to the data file that needs to be scaled.",
)
def scale(data_path: str) -> None:
    """Scales the data found at the specified path.

    Args:
        data_path (str): The path to the data file that needs to be scaled.
    """

    df = pd.read_csv(data_path)

    scaler = StandardScaler()
    df.iloc[:, :-1] = scaler.fit_transform(df.iloc[:, :-1])

    df.to_csv(data_path, index=False)


if __name__ == "__main__":
    scale()
